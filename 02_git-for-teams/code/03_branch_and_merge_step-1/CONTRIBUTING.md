# Contribution Guidelines

1. Fork the project.
1. Extend the code.
1. Make sure that current example files still work properly and provide new
   examples for added cases and features.
1. Create a merge request.
