## License Hint

Copyright © 2020 German Aerospace Center (DLR)

This work is licensed under multiple licenses:
- The main content is licensed under [CC-BY-4.0](LICENSES/CC-BY-4.0.txt).
- All code is licensed under [MIT](LICENSES/MIT.txt).
- Insignificant files are licensed under [CC0-1.0](LICENSES/CC0-1.0.txt).

- The powerpoint slides may contain content under other licenses. Please see the `Image Credit` slide for details.
 
Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
